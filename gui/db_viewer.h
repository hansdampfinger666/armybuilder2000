#pragma once

#include <QDialogButtonBox>
#include <QGridLayout>
#include <QPushButton>
#include <QStandardItemModel>
#include <QTableView>
#include <QWidget>

#include <database/database.h>
#include <gui/add_dataset.h>
#include <guitools/qt_generate.h>
#include <tools/types.h>

#include <iostream>
#include <tools/print.h>

class DatabaseViewer : public QWidget
{
  Q_OBJECT

public:
  struct ids_and_rows
  {
    vector<u32> ids_;
    vector<u32> rows_;
  };

  DatabaseViewer(QWidget* parent, const Db* db);
  ~DatabaseViewer();

public slots:
  void fetch_new_db_entry(const u32 id);

private:
  QWidget* parent_ = nullptr;
  const Db* db_ = nullptr;
  QTableView* table_view_ = nullptr;
  QStandardItemModel* table_model_ = nullptr;
  QGridLayout* layout_ = nullptr;

  DBTypes active_view_{ 0 };
  vector<u32> selected_ids_;

  void switch_tables(const DBTypes db_type);
  void set_window_title(const DBTypes new_view);
  void add_dataset();
  void delete_datasets();
  opt<i32> get_id_field_position();
  ids_and_rows get_selected_ids_and_rows();
  ids_and_rows extract_ids_and_rows_from_selection(const QTableView* table_view,
                                                   const u32 id_field_position);
  opt<i32> get_id_from_model_index(const QModelIndex& model_index);
  void element_dc(const QModelIndex& model_index);
};
